﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using SendBulkEmail.DAO;
using MySql.Data.MySqlClient;
using SendBulkEmail.DTO;


namespace SendBulkEmail.GUI
{
    public partial class Login : Form
    {

        User_DAO _User = new User_DAO();
        public Login()
        {
            InitializeComponent();
        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if (_User.login(txbEmail.Text, txbMatKhau.Text) == true)
            {
                Sender _sender = new Sender(txbEmail.Text);
                this.Hide();
                txbMatKhau.Clear();
                txbEmail.Clear();
                _sender.ShowDialog();
                this.Show();
            }
            else
            {
                MessageBox.Show("Sai Email hoặc mật khẩu!");
            }
        }

        private void btnDangKi_Click(object sender, EventArgs e)
        {
            DangKi f = new DangKi();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
