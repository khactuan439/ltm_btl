﻿
namespace SendBulkEmail.GUI
{
    partial class DangKi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DangKi));
            this.txbEmail_dk = new System.Windows.Forms.TextBox();
            this.btnDangKi_dk = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.cbbGioiTinh = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txbHoTen = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txbMatKhauEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txbNhapLai_MK = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txbMatKhau_dk = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txbSoDienThoai = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_LayMK_Email = new System.Windows.Forms.Button();
            this.btnThoat_dk = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txbEmail_dk
            // 
            this.txbEmail_dk.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbEmail_dk.Location = new System.Drawing.Point(160, 125);
            this.txbEmail_dk.Margin = new System.Windows.Forms.Padding(2);
            this.txbEmail_dk.Name = "txbEmail_dk";
            this.txbEmail_dk.Size = new System.Drawing.Size(212, 23);
            this.txbEmail_dk.TabIndex = 45;
            // 
            // btnDangKi_dk
            // 
            this.btnDangKi_dk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDangKi_dk.ForeColor = System.Drawing.Color.Black;
            this.btnDangKi_dk.Location = new System.Drawing.Point(146, 524);
            this.btnDangKi_dk.Margin = new System.Windows.Forms.Padding(2);
            this.btnDangKi_dk.Name = "btnDangKi_dk";
            this.btnDangKi_dk.Size = new System.Drawing.Size(97, 31);
            this.btnDangKi_dk.TabIndex = 44;
            this.btnDangKi_dk.Text = "Đăng kí";
            this.btnDangKi_dk.UseVisualStyleBackColor = true;
            this.btnDangKi_dk.Click += new System.EventHandler(this.btnDangKi_dk_Click_1);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(5, 387);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 17);
            this.label17.TabIndex = 28;
            this.label17.Text = "Số điện thoại";
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(160, 345);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(212, 23);
            this.dtpNgaySinh.TabIndex = 42;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(5, 345);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 17);
            this.label16.TabIndex = 30;
            this.label16.Text = "Ngày sinh";
            // 
            // cbbGioiTinh
            // 
            this.cbbGioiTinh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbGioiTinh.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbGioiTinh.FormattingEnabled = true;
            this.cbbGioiTinh.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbbGioiTinh.Items.AddRange(new object[] {
            "Nam",
            "Nữ"});
            this.cbbGioiTinh.Location = new System.Drawing.Point(160, 298);
            this.cbbGioiTinh.Name = "cbbGioiTinh";
            this.cbbGioiTinh.Size = new System.Drawing.Size(212, 23);
            this.cbbGioiTinh.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(5, 305);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 17);
            this.label15.TabIndex = 31;
            this.label15.Text = "Giới tính";
            // 
            // txbHoTen
            // 
            this.txbHoTen.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbHoTen.Location = new System.Drawing.Point(160, 256);
            this.txbHoTen.Margin = new System.Windows.Forms.Padding(2);
            this.txbHoTen.Name = "txbHoTen";
            this.txbHoTen.Size = new System.Drawing.Size(212, 23);
            this.txbHoTen.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 258);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 33;
            this.label14.Text = "Họ tên";
            // 
            // txbMatKhauEmail
            // 
            this.txbMatKhauEmail.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbMatKhauEmail.Location = new System.Drawing.Point(200, 451);
            this.txbMatKhauEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txbMatKhauEmail.Name = "txbMatKhauEmail";
            this.txbMatKhauEmail.PasswordChar = '*';
            this.txbMatKhauEmail.Size = new System.Drawing.Size(172, 23);
            this.txbMatKhauEmail.TabIndex = 37;
            this.txbMatKhauEmail.UseSystemPasswordChar = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(5, 452);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(191, 17);
            this.label13.TabIndex = 34;
            this.label13.Text = "Mật khẩu ứng dụng Email";
            // 
            // txbNhapLai_MK
            // 
            this.txbNhapLai_MK.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbNhapLai_MK.Location = new System.Drawing.Point(160, 217);
            this.txbNhapLai_MK.Margin = new System.Windows.Forms.Padding(2);
            this.txbNhapLai_MK.Name = "txbNhapLai_MK";
            this.txbNhapLai_MK.PasswordChar = '*';
            this.txbNhapLai_MK.Size = new System.Drawing.Size(212, 23);
            this.txbNhapLai_MK.TabIndex = 38;
            this.txbNhapLai_MK.UseSystemPasswordChar = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(5, 217);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(147, 17);
            this.label12.TabIndex = 35;
            this.label12.Text = "Xác nhận mật khẩu";
            // 
            // txbMatKhau_dk
            // 
            this.txbMatKhau_dk.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbMatKhau_dk.Location = new System.Drawing.Point(160, 172);
            this.txbMatKhau_dk.Margin = new System.Windows.Forms.Padding(2);
            this.txbMatKhau_dk.Name = "txbMatKhau_dk";
            this.txbMatKhau_dk.PasswordChar = '*';
            this.txbMatKhau_dk.Size = new System.Drawing.Size(212, 23);
            this.txbMatKhau_dk.TabIndex = 36;
            this.txbMatKhau_dk.UseSystemPasswordChar = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(5, 172);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "Mật khẩu";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(5, 125);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 17);
            this.label10.TabIndex = 29;
            this.label10.Text = "Email";
            // 
            // txbSoDienThoai
            // 
            this.txbSoDienThoai.Font = new System.Drawing.Font("Segoe UI", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbSoDienThoai.Location = new System.Drawing.Point(160, 384);
            this.txbSoDienThoai.Margin = new System.Windows.Forms.Padding(2);
            this.txbSoDienThoai.Name = "txbSoDienThoai";
            this.txbSoDienThoai.Size = new System.Drawing.Size(212, 23);
            this.txbSoDienThoai.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(79, 67);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 24);
            this.label5.TabIndex = 39;
            this.label5.Text = "ĐĂNG KÍ TÀI KHOẢN";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_LayMK_Email);
            this.panel1.Controls.Add(this.dtpNgaySinh);
            this.panel1.Controls.Add(this.txbEmail_dk);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txbMatKhauEmail);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txbSoDienThoai);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txbMatKhau_dk);
            this.panel1.Controls.Add(this.cbbGioiTinh);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txbNhapLai_MK);
            this.panel1.Controls.Add(this.txbHoTen);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Location = new System.Drawing.Point(32, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 492);
            this.panel1.TabIndex = 46;
            // 
            // btn_LayMK_Email
            // 
            this.btn_LayMK_Email.Location = new System.Drawing.Point(187, 423);
            this.btn_LayMK_Email.Name = "btn_LayMK_Email";
            this.btn_LayMK_Email.Size = new System.Drawing.Size(185, 23);
            this.btn_LayMK_Email.TabIndex = 46;
            this.btn_LayMK_Email.Text = "Lấy mật khẩu ứng dụng Email";
            this.btn_LayMK_Email.UseVisualStyleBackColor = true;
            this.btn_LayMK_Email.Click += new System.EventHandler(this.btn_LayMK_Email_Click);
            // 
            // btnThoat_dk
            // 
            this.btnThoat_dk.Location = new System.Drawing.Point(274, 524);
            this.btnThoat_dk.Name = "btnThoat_dk";
            this.btnThoat_dk.Size = new System.Drawing.Size(98, 31);
            this.btnThoat_dk.TabIndex = 47;
            this.btnThoat_dk.Text = "Thoát";
            this.btnThoat_dk.UseVisualStyleBackColor = true;
            this.btnThoat_dk.Click += new System.EventHandler(this.btnThoat_dk_Click);
            // 
            // DangKi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(526, 675);
            this.Controls.Add(this.btnThoat_dk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDangKi_dk);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DangKi";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txbEmail_dk;
        private System.Windows.Forms.Button btnDangKi_dk;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbbGioiTinh;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txbHoTen;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txbMatKhauEmail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txbNhapLai_MK;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txbMatKhau_dk;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txbSoDienThoai;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnThoat_dk;
        private System.Windows.Forms.Button btn_LayMK_Email;
    }
}