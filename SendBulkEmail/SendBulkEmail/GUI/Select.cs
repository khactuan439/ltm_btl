﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SendBulkEmail.DAO;

namespace SendBulkEmail.GUI
{
    public partial class Select : Form
    {
        private string sender = "";
        public List<string> ListEmailReceiver = new List<string>();
        MailReceiver_DAO listReceiver = new MailReceiver_DAO();
        
        public Select(string sender)
        {
            InitializeComponent();
            this.sender = sender;
            loadListEmail();
        }
        void loadListEmail()
        {
            DataTable list = listReceiver.getListEmail(this.sender);

            for (int i = 0; i < list.Rows.Count; i++)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = list.Rows[i]["email_address"].ToString();
                temp.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = list.Rows[i]["group_name"].ToString() });
                lv_ListEmail.Items.Add(temp);
            }
        }

        private void btnXong_Click(object sender, EventArgs e)
        {
            ListEmailReceiver.Clear();
            for (int i =0; i< lv_ListEmail.CheckedItems.Count; i++)
            {
                ListEmailReceiver.Add(lv_ListEmail.CheckedItems[i].Text);
            }
            this.Hide();
        }

        private void txb_TimKiem_TextChanged(object sender, EventArgs e)
        {
            DataTable search = listReceiver.SearchEmail(this.sender, txb_TimKiem.Text,cbb_Nhom.SelectedItem.ToString());
            lv_ListEmail.Items.Clear();
            for (int i = 0; i < search.Rows.Count; i++)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = search.Rows[i]["email_address"].ToString();
                temp.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = search.Rows[i]["group_name"].ToString() });
                lv_ListEmail.Items.Add(temp);
            }
        }

        private void ckb_SelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if(ckb_SelectAll.Checked==true)
            {
                for (int i=0; i< lv_ListEmail.Items.Count; i++)
                {
                    lv_ListEmail.Items[i].Checked = true;
                }
            }
            else
            {
                for (int i = 0; i < lv_ListEmail.Items.Count; i++)
                {
                    lv_ListEmail.Items[i].Checked = false;
                }
            }
        }

        

        private void cbb_Nhom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
