﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SendBulkEmail.DAO;
using SendBulkEmail.DTO;

namespace SendBulkEmail.GUI
{
    public partial class DangKi : Form
    {
        public DangKi()
        {
            InitializeComponent();
        }
      

        private void btnDangKi_dk_Click_1(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            if (string.IsNullOrEmpty(txbEmail_dk.Text) || string.IsNullOrEmpty(txbMatKhau_dk.Text) || string.IsNullOrEmpty(txbNhapLai_MK.Text) || string.IsNullOrEmpty(txbMatKhauEmail.Text)
                || string.IsNullOrEmpty(txbHoTen.Text) || string.IsNullOrEmpty(cbbGioiTinh.Text) || string.IsNullOrEmpty(dtpNgaySinh.Text) || string.IsNullOrEmpty(txbSoDienThoai.Text))
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin!");
                return;
            }
            if (MailReceiver_DAO.ValidateEmail(txbEmail_dk.Text) == false)
            {
                MessageBox.Show("Email không đúng", "Fail",
                   MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

             if (txbMatKhau_dk.Text != txbNhapLai_MK.Text)
            {
                MessageBox.Show("Xác nhận mật khẩu sai!");
                return;
            }
            else if (dtpNgaySinh.Value.Year < now.Year || (dtpNgaySinh.Value.Year == now.Year && dtpNgaySinh.Value.Month < now.Month) || (dtpNgaySinh.Value.Year == now.Year && dtpNgaySinh.Value.Month == now.Month && dtpNgaySinh.Value.Day <= now.Day))
            {

                User user = new User(txbEmail_dk.Text, txbMatKhau_dk.Text, txbMatKhauEmail.Text, txbHoTen.Text, cbbGioiTinh.SelectedItem.ToString(), dtpNgaySinh.Value, txbSoDienThoai.Text);
                User_DAO ud = new User_DAO();
                if (ud.search(user.Email) == false)
                {
                    ud.signUp(user);
                    MessageBox.Show("Đăng kí tài khoản thành công!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Tên đăng nhập đã tồn tại!");
                }
            }
            else
            {
                MessageBox.Show("Ngày sinh không hợp lệ!");
            }
        }

        private void btnThoat_dk_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_LayMK_Email_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = "pdf";
            dialog.FileName = "Mật Khẩu Ứng Dụng Gmail";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                File.Copy("Mật Khẩu Ứng Dụng Gmail.pdf", dialog.FileName);
            }
            
        }
    }
}
