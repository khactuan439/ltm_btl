﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using SendBulkEmail.DTO;
using SendBulkEmail.DAO;
using System.IO;
using System.Net.Mail;

namespace SendBulkEmail.GUI
{
    public partial class Sender : Form
    {
        List<string> listEmail = new List<string>();// danh sách email người nhận
        EmailContent message = new EmailContent();// thông tin thư gửi đi
        MailReceiver_DAO receiver_DAO = new MailReceiver_DAO();
        User_DAO user_DAO = new User_DAO();
        User user = new User();
        string emailUser = "";

        public Sender(string emailUser)
        {
            InitializeComponent();
            this.emailUser = emailUser;
            user = user_DAO.GetUser(this.emailUser);
            loadReceiver();
            Load_ThongTinUser();
        }

        private void loadReceiver()
        {
            DataTable list = receiver_DAO.getListEmail(this.emailUser);
            lv_ListReceiver.Items.Clear();
            for (int i = 0; i < list.Rows.Count; i++)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = list.Rows[i]["email_address"].ToString();
                temp.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = list.Rows[i]["group_name"].ToString() });
                lv_ListReceiver.Items.Add(temp);
            }
            load_Combobox();
        }

        private void Load_ThongTinUser()
        {
            txbEmail.Text = user.Email;
            txbHoTen.Text = user.Name;
            cbbSex.SelectedItem = user.Sex;
            dtp_NgaySinh.Value = user.Birhday;
            txb_SDT.Text = user.Phone;
        }
        private void btn_SeclectFile_Click(object sender, EventArgs e)
        {
            //chọn file
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lb_File.Text = dialog.FileName;
            }
        }

        private void btn_SeclectReceiver_Click(object sender, EventArgs e)
        {
            Select seclectEmail = new Select(this.emailUser);
            seclectEmail.ShowDialog();
            listEmail = seclectEmail.ListEmailReceiver;
            lv_EmailSeclected.Items.Clear();
            foreach (var item in listEmail)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = item;
                lv_EmailSeclected.Items.Add(temp);
            }
            label_CountEmail.Text = "Số lượng : " + listEmail.Count;
        }
        private void btn_Send_Click(object sender, EventArgs e)
        {
            if (!(listEmail.Count > 0))//kiểm tra danh sách email trống hay không
            {
                MessageBox.Show("Vui long chọn người nhận!");
            }
            else
            {
                message.Subject = txbSubject.Text;
                message.Body = txbMessage.Text;
                message.To = listEmail;
                message.From =this.emailUser;
                try
                {
                    message.Attachment = new Attachment(lb_File.Text);
                }
                catch { }
                List<MailMessage> listMessage = message.mail();
                //gửi email
                Thread threadd = new Thread(() =>
                {
                    //lặp danh sách email người nhận
                    int count = 0;
                    foreach (var item in listMessage)
                    {
                        user.SendMail(item, ref count);
                    }
                    int thanhcong = listMessage.Count - count;
                    notifyIcon_SendMail.ShowBalloonTip(5000,"Thông báo tình trạng gửi mail!", string.Format("Gửi thành công: {0} Email!  Gửi thất bại : {1} Email!", thanhcong, count), ToolTipIcon.None);
                });
                threadd.Start();
                txbSubject.Text = "";
                txbMessage.Text = "";
                lb_File.Text = "";
                lv_EmailSeclected.Clear();
            }
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxEmailReceiver.Text))
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
            else
            {
                MailReceiver receiver;
                if (string.IsNullOrEmpty(cbbGroup.Text))
                    receiver = new MailReceiver(tbxEmailReceiver.Text, this.emailUser,"Không có");
                else
                    receiver = new MailReceiver(tbxEmailReceiver.Text, this.emailUser, cbbGroup.Text);
                if (!receiver_DAO.Insert(receiver))
                    MessageBox.Show("Email đã tồn tại!");
                else MessageBox.Show("Thêm thành công");
            }
            loadReceiver();
        }

        private void btnThemTuFile_Click(object sender, EventArgs e)
        {
            List<MailReceiver> themEmail = new List<MailReceiver>();
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                List<string> temp = receiver_DAO.ReadFile(dialog.FileName);
                foreach (var item in temp)
                {
                    MailReceiver m = new MailReceiver(item, this.emailUser, "Không có");
                    themEmail.Add(m);
                }
            }
            int count = 0;
            receiver_DAO.InsertList(themEmail,ref count);
            MessageBox.Show("Thêm thành công " + count + " Email");
            loadReceiver();
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxEmailReceiver.Text) || string.IsNullOrEmpty(cbbGroup.Text))
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
            else
            {
                MailReceiver receiver = new MailReceiver(tbxEmailReceiver.Text, this.emailUser, cbbGroup.Text);
                if (!receiver_DAO.Update(receiver))
                    MessageBox.Show("Email chưa tồn tại không thể cập nhật!");
                else MessageBox.Show("Cập nhật thành công");
                loadReceiver();
            }
        }
        private void btn_ChonTuFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                listEmail = receiver_DAO.ReadFile(dialog.FileName);
            }
            lv_EmailSeclected.Items.Clear();
            foreach (var item in listEmail)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = item;
                lv_EmailSeclected.Items.Add(temp);
            }
            label_CountEmail.Text = "Số lượng : " + listEmail.Count;
        }

        private void tbxTimKiem_TextChanged(object sender, EventArgs e)
        {
            DataTable search = receiver_DAO.SearchEmail(this.user.Email, tbxTimKiem.Text,cbbGroup_Loc.SelectedItem.ToString());
            lv_ListReceiver.Items.Clear();
            for (int i = 0; i < search.Rows.Count; i++)
            {
                ListViewItem temp = new ListViewItem();
                temp.Text = search.Rows[i]["email_address"].ToString();
                temp.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = search.Rows[i]["group_name"].ToString() });
                lv_ListReceiver.Items.Add(temp);
            }
            label_CountReceiver.Text = "Số lượng : " + lv_ListReceiver.Items.Count;
        }
        void load_Combobox()
        {
            DataTable data = receiver_DAO.getListGroup(this.user.Email);
            cbbGroup.Items.Clear();
            cbbGroup_Loc.Items.Clear();
            cbbGroup.Items.Add("Tất cả");
            cbbGroup_Loc.Items.Add("Tất cả");
            foreach (DataRow item in data.Rows)
            {
                string temp = item["group_name"].ToString();
                cbbGroup.Items.Add(temp);
                cbbGroup_Loc.Items.Add(temp);
            }
            cbbGroup.SelectedIndex = 0;
            cbbGroup_Loc.SelectedIndex = 0;
        }

        private void lv_ListReceiver_MouseClick(object sender, MouseEventArgs e)
        {

            tbxEmailReceiver.Text = lv_ListReceiver.SelectedItems[0].SubItems[0].Text;
            cbbGroup.Text = lv_ListReceiver.SelectedItems[0].SubItems[1].Text;

        }

        private void btnCapNhatThongTin_Click(object sender, EventArgs e)
        {
            user.Name = txbHoTen.Text;
            user.Sex = cbbSex.SelectedItem.ToString();
            user.Birhday = dtp_NgaySinh.Value;
            user.Phone= txb_SDT.Text;
            if (user_DAO.Update(user))
            {
                MessageBox.Show("Cập nhật thông tin thành công!");
                Load_ThongTinUser();
            }
            else
                MessageBox.Show("Cập nhật thông tin Không thành công!");
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxEmailReceiver.Text) || string.IsNullOrEmpty(cbbGroup.Text))
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
            else
            {
                MailReceiver receiver = new MailReceiver(tbxEmailReceiver.Text, this.emailUser, cbbGroup.Text);
                if (!receiver_DAO.Delete(receiver))
                    MessageBox.Show("Người nhận Không tồn tại trên hệ thống!");
                else MessageBox.Show("Xóa người nhận thành công!");
                loadReceiver();
            }
        }

        private void cbbGroup_Loc_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxTimKiem_TextChanged(sender,e);
        }
    }
}
