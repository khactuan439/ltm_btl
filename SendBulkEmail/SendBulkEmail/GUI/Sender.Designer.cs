﻿
namespace SendBulkEmail.GUI
{
    partial class Sender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sender));
            this.btn_Send = new System.Windows.Forms.Button();
            this.btn_SeclectReceiver = new System.Windows.Forms.Button();
            this.btn_SeclectFile = new System.Windows.Forms.Button();
            this.label_CountEmail = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txbSubject = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.tab_QuanLy = new System.Windows.Forms.TabPage();
            this.lv_ListReceiver = new System.Windows.Forms.ListView();
            this.column_Email = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.column_Group = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.cbbGroup_Loc = new System.Windows.Forms.ComboBox();
            this.tbxTimKiem = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnThemTuFile = new System.Windows.Forms.Button();
            this.btn_Them = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnCapNhat = new System.Windows.Forms.Button();
            this.tbxEmailReceiver = new System.Windows.Forms.TextBox();
            this.cbbGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tab_Info = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCapNhatThongTin = new System.Windows.Forms.Button();
            this.btn_DoiMatKhau = new System.Windows.Forms.Button();
            this.dtp_NgaySinh = new System.Windows.Forms.DateTimePicker();
            this.cbbSex = new System.Windows.Forms.ComboBox();
            this.txb_SDT = new System.Windows.Forms.TextBox();
            this.txbHoTen = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txbEmail = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_SoanThu = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lb_File = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lv_EmailSeclected = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label_So_Luong = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_ChọnTuFile = new System.Windows.Forms.Button();
            this.txbMessage = new System.Windows.Forms.TextBox();
            this.notifyIcon_SendMail = new System.Windows.Forms.NotifyIcon(this.components);
            this.label_CountReceiver = new System.Windows.Forms.Label();
            this.tab_QuanLy.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tab_Info.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tab_SoanThu.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Send
            // 
            this.btn_Send.Location = new System.Drawing.Point(349, 613);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(156, 36);
            this.btn_Send.TabIndex = 50;
            this.btn_Send.Text = "Gửi";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // btn_SeclectReceiver
            // 
            this.btn_SeclectReceiver.Location = new System.Drawing.Point(19, 51);
            this.btn_SeclectReceiver.Name = "btn_SeclectReceiver";
            this.btn_SeclectReceiver.Size = new System.Drawing.Size(92, 25);
            this.btn_SeclectReceiver.TabIndex = 51;
            this.btn_SeclectReceiver.Text = "Chọn";
            this.btn_SeclectReceiver.UseVisualStyleBackColor = true;
            this.btn_SeclectReceiver.Click += new System.EventHandler(this.btn_SeclectReceiver_Click);
            // 
            // btn_SeclectFile
            // 
            this.btn_SeclectFile.Location = new System.Drawing.Point(14, 66);
            this.btn_SeclectFile.Name = "btn_SeclectFile";
            this.btn_SeclectFile.Size = new System.Drawing.Size(69, 35);
            this.btn_SeclectFile.TabIndex = 49;
            this.btn_SeclectFile.Text = "Chọn file";
            this.btn_SeclectFile.UseVisualStyleBackColor = true;
            this.btn_SeclectFile.Click += new System.EventHandler(this.btn_SeclectFile_Click);
            // 
            // label_CountEmail
            // 
            this.label_CountEmail.AutoSize = true;
            this.label_CountEmail.Location = new System.Drawing.Point(19, 159);
            this.label_CountEmail.Name = "label_CountEmail";
            this.label_CountEmail.Size = new System.Drawing.Size(62, 15);
            this.label_CountEmail.TabIndex = 44;
            this.label_CountEmail.Text = "Số lượng :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 46;
            this.label1.Text = "Tệp đính kèm";
            // 
            // txbSubject
            // 
            this.txbSubject.Location = new System.Drawing.Point(113, 9);
            this.txbSubject.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txbSubject.Name = "txbSubject";
            this.txbSubject.Size = new System.Drawing.Size(650, 21);
            this.txbSubject.TabIndex = 35;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(43, 12);
            this.lblSubject.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(52, 15);
            this.lblSubject.TabIndex = 33;
            this.lblSubject.Text = "Chủ đề :";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(44, 87);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(63, 15);
            this.lblMessage.TabIndex = 40;
            this.lblMessage.Text = "Nội dung :";
            // 
            // tab_QuanLy
            // 
            this.tab_QuanLy.Controls.Add(this.lv_ListReceiver);
            this.tab_QuanLy.Controls.Add(this.panel1);
            this.tab_QuanLy.Location = new System.Drawing.Point(4, 24);
            this.tab_QuanLy.Name = "tab_QuanLy";
            this.tab_QuanLy.Padding = new System.Windows.Forms.Padding(3);
            this.tab_QuanLy.Size = new System.Drawing.Size(832, 774);
            this.tab_QuanLy.TabIndex = 1;
            this.tab_QuanLy.Text = "Quản lý người nhận";
            this.tab_QuanLy.UseVisualStyleBackColor = true;
            // 
            // lv_ListReceiver
            // 
            this.lv_ListReceiver.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.column_Email,
            this.column_Group});
            this.lv_ListReceiver.FullRowSelect = true;
            this.lv_ListReceiver.HideSelection = false;
            this.lv_ListReceiver.Location = new System.Drawing.Point(103, 352);
            this.lv_ListReceiver.Name = "lv_ListReceiver";
            this.lv_ListReceiver.Size = new System.Drawing.Size(614, 296);
            this.lv_ListReceiver.TabIndex = 1;
            this.lv_ListReceiver.UseCompatibleStateImageBehavior = false;
            this.lv_ListReceiver.View = System.Windows.Forms.View.Details;
            this.lv_ListReceiver.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lv_ListReceiver_MouseClick);
            // 
            // column_Email
            // 
            this.column_Email.Text = "Email người nhận";
            this.column_Email.Width = 132;
            // 
            // column_Group
            // 
            this.column_Group.Text = "Nhóm";
            this.column_Group.Width = 116;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(91, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(636, 310);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label_CountReceiver);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.cbbGroup_Loc);
            this.panel3.Controls.Add(this.tbxTimKiem);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.btnXoa);
            this.panel3.Controls.Add(this.btnThemTuFile);
            this.panel3.Controls.Add(this.btn_Them);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.btnCapNhat);
            this.panel3.Controls.Add(this.tbxEmailReceiver);
            this.panel3.Controls.Add(this.cbbGroup);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(12, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(614, 281);
            this.panel3.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 140);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(186, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "Thêm người nhận bằng file (.txt) :";
            // 
            // cbbGroup_Loc
            // 
            this.cbbGroup_Loc.FormattingEnabled = true;
            this.cbbGroup_Loc.Location = new System.Drawing.Point(443, 217);
            this.cbbGroup_Loc.Name = "cbbGroup_Loc";
            this.cbbGroup_Loc.Size = new System.Drawing.Size(136, 23);
            this.cbbGroup_Loc.TabIndex = 1;
            this.cbbGroup_Loc.SelectedIndexChanged += new System.EventHandler(this.cbbGroup_Loc_SelectedIndexChanged);
            // 
            // tbxTimKiem
            // 
            this.tbxTimKiem.Location = new System.Drawing.Point(86, 217);
            this.tbxTimKiem.Name = "tbxTimKiem";
            this.tbxTimKiem.Size = new System.Drawing.Size(229, 21);
            this.tbxTimKiem.TabIndex = 2;
            this.tbxTimKiem.TextChanged += new System.EventHandler(this.tbxTimKiem_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tìm kiếm";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(375, 134);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(87, 27);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThemTuFile
            // 
            this.btnThemTuFile.Location = new System.Drawing.Point(212, 134);
            this.btnThemTuFile.Name = "btnThemTuFile";
            this.btnThemTuFile.Size = new System.Drawing.Size(87, 27);
            this.btnThemTuFile.TabIndex = 3;
            this.btnThemTuFile.Text = "Thêm từ file";
            this.btnThemTuFile.UseVisualStyleBackColor = true;
            this.btnThemTuFile.Click += new System.EventHandler(this.btnThemTuFile_Click);
            // 
            // btn_Them
            // 
            this.btn_Them.Location = new System.Drawing.Point(212, 90);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(87, 27);
            this.btn_Them.TabIndex = 3;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.UseVisualStyleBackColor = true;
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(342, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Lọc theo nhóm :";
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Location = new System.Drawing.Point(375, 90);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(87, 27);
            this.btnCapNhat.TabIndex = 3;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.UseVisualStyleBackColor = true;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // tbxEmailReceiver
            // 
            this.tbxEmailReceiver.Location = new System.Drawing.Point(141, 46);
            this.tbxEmailReceiver.Name = "tbxEmailReceiver";
            this.tbxEmailReceiver.Size = new System.Drawing.Size(116, 21);
            this.tbxEmailReceiver.TabIndex = 2;
            // 
            // cbbGroup
            // 
            this.cbbGroup.FormattingEnabled = true;
            this.cbbGroup.Location = new System.Drawing.Point(394, 45);
            this.cbbGroup.Name = "cbbGroup";
            this.cbbGroup.Size = new System.Drawing.Size(140, 23);
            this.cbbGroup.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(329, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nhóm :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Người nhận";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Địa chỉ Email :";
            // 
            // tab_Info
            // 
            this.tab_Info.Controls.Add(this.panel5);
            this.tab_Info.Location = new System.Drawing.Point(4, 24);
            this.tab_Info.Name = "tab_Info";
            this.tab_Info.Size = new System.Drawing.Size(832, 774);
            this.tab_Info.TabIndex = 2;
            this.tab_Info.Text = "Thông tin cá nhân";
            this.tab_Info.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btnCapNhatThongTin);
            this.panel5.Controls.Add(this.btn_DoiMatKhau);
            this.panel5.Controls.Add(this.dtp_NgaySinh);
            this.panel5.Controls.Add(this.cbbSex);
            this.panel5.Controls.Add(this.txb_SDT);
            this.panel5.Controls.Add(this.txbHoTen);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.txbEmail);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Location = new System.Drawing.Point(175, 40);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(464, 497);
            this.panel5.TabIndex = 0;
            // 
            // btnCapNhatThongTin
            // 
            this.btnCapNhatThongTin.Location = new System.Drawing.Point(217, 436);
            this.btnCapNhatThongTin.Name = "btnCapNhatThongTin";
            this.btnCapNhatThongTin.Size = new System.Drawing.Size(147, 27);
            this.btnCapNhatThongTin.TabIndex = 4;
            this.btnCapNhatThongTin.Text = "Cập nhật Thông tin";
            this.btnCapNhatThongTin.UseVisualStyleBackColor = true;
            this.btnCapNhatThongTin.Click += new System.EventHandler(this.btnCapNhatThongTin_Click);
            // 
            // btn_DoiMatKhau
            // 
            this.btn_DoiMatKhau.Location = new System.Drawing.Point(40, 436);
            this.btn_DoiMatKhau.Name = "btn_DoiMatKhau";
            this.btn_DoiMatKhau.Size = new System.Drawing.Size(117, 27);
            this.btn_DoiMatKhau.TabIndex = 4;
            this.btn_DoiMatKhau.Text = "Đổi mật khẩu";
            this.btn_DoiMatKhau.UseVisualStyleBackColor = true;
            // 
            // dtp_NgaySinh
            // 
            this.dtp_NgaySinh.CustomFormat = "dd/MM/yyyy";
            this.dtp_NgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_NgaySinh.Location = new System.Drawing.Point(140, 249);
            this.dtp_NgaySinh.Name = "dtp_NgaySinh";
            this.dtp_NgaySinh.Size = new System.Drawing.Size(178, 21);
            this.dtp_NgaySinh.TabIndex = 3;
            // 
            // cbbSex
            // 
            this.cbbSex.FormattingEnabled = true;
            this.cbbSex.Items.AddRange(new object[] {
            "Nam",
            "Nữ",
            "Khác"});
            this.cbbSex.Location = new System.Drawing.Point(140, 190);
            this.cbbSex.Name = "cbbSex";
            this.cbbSex.Size = new System.Drawing.Size(178, 23);
            this.cbbSex.TabIndex = 2;
            // 
            // txb_SDT
            // 
            this.txb_SDT.Location = new System.Drawing.Point(140, 323);
            this.txb_SDT.Name = "txb_SDT";
            this.txb_SDT.Size = new System.Drawing.Size(178, 21);
            this.txb_SDT.TabIndex = 1;
            // 
            // txbHoTen
            // 
            this.txbHoTen.Location = new System.Drawing.Point(140, 121);
            this.txbHoTen.Name = "txbHoTen";
            this.txbHoTen.Size = new System.Drawing.Size(178, 21);
            this.txbHoTen.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 327);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Số điện thoại";
            // 
            // txbEmail
            // 
            this.txbEmail.Location = new System.Drawing.Point(140, 68);
            this.txbEmail.Name = "txbEmail";
            this.txbEmail.ReadOnly = true;
            this.txbEmail.Size = new System.Drawing.Size(178, 21);
            this.txbEmail.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 249);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 15);
            this.label15.TabIndex = 0;
            this.label15.Text = "Ngày sinh";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(36, 190);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Giới tính";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "Họ Tên";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Email";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(169, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Thông tin cá nhân";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_SoanThu);
            this.tabControl1.Controls.Add(this.tab_QuanLy);
            this.tabControl1.Controls.Add(this.tab_Info);
            this.tabControl1.Location = new System.Drawing.Point(14, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(840, 802);
            this.tabControl1.TabIndex = 1;
            // 
            // tab_SoanThu
            // 
            this.tab_SoanThu.Controls.Add(this.txbSubject);
            this.tab_SoanThu.Controls.Add(this.lblSubject);
            this.tab_SoanThu.Controls.Add(this.panel6);
            this.tab_SoanThu.Controls.Add(this.panel4);
            this.tab_SoanThu.Controls.Add(this.btn_Send);
            this.tab_SoanThu.Controls.Add(this.txbMessage);
            this.tab_SoanThu.Controls.Add(this.lblMessage);
            this.tab_SoanThu.Location = new System.Drawing.Point(4, 24);
            this.tab_SoanThu.Name = "tab_SoanThu";
            this.tab_SoanThu.Padding = new System.Windows.Forms.Padding(3);
            this.tab_SoanThu.Size = new System.Drawing.Size(832, 774);
            this.tab_SoanThu.TabIndex = 0;
            this.tab_SoanThu.Text = "Soạn thư";
            this.tab_SoanThu.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lb_File);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.btn_SeclectFile);
            this.panel6.Location = new System.Drawing.Point(114, 353);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(287, 226);
            this.panel6.TabIndex = 54;
            // 
            // lb_File
            // 
            this.lb_File.AutoSize = true;
            this.lb_File.Location = new System.Drawing.Point(134, 76);
            this.lb_File.Name = "lb_File";
            this.lb_File.Size = new System.Drawing.Size(0, 15);
            this.lb_File.TabIndex = 50;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lv_EmailSeclected);
            this.panel4.Controls.Add(this.label_So_Luong);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.btn_ChọnTuFile);
            this.panel4.Controls.Add(this.label_CountEmail);
            this.panel4.Controls.Add(this.btn_SeclectReceiver);
            this.panel4.Location = new System.Drawing.Point(457, 353);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(306, 226);
            this.panel4.TabIndex = 53;
            // 
            // lv_EmailSeclected
            // 
            this.lv_EmailSeclected.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lv_EmailSeclected.HideSelection = false;
            this.lv_EmailSeclected.Location = new System.Drawing.Point(119, 51);
            this.lv_EmailSeclected.Name = "lv_EmailSeclected";
            this.lv_EmailSeclected.Size = new System.Drawing.Size(181, 146);
            this.lv_EmailSeclected.TabIndex = 52;
            this.lv_EmailSeclected.UseCompatibleStateImageBehavior = false;
            this.lv_EmailSeclected.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 149;
            // 
            // label_So_Luong
            // 
            this.label_So_Luong.AutoSize = true;
            this.label_So_Luong.Location = new System.Drawing.Point(98, 160);
            this.label_So_Luong.Name = "label_So_Luong";
            this.label_So_Luong.Size = new System.Drawing.Size(0, 15);
            this.label_So_Luong.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 44;
            this.label5.Text = "Người nhận";
            // 
            // btn_ChọnTuFile
            // 
            this.btn_ChọnTuFile.Location = new System.Drawing.Point(19, 95);
            this.btn_ChọnTuFile.Name = "btn_ChọnTuFile";
            this.btn_ChọnTuFile.Size = new System.Drawing.Size(92, 27);
            this.btn_ChọnTuFile.TabIndex = 51;
            this.btn_ChọnTuFile.Text = "Chọn từ file";
            this.btn_ChọnTuFile.UseVisualStyleBackColor = true;
            this.btn_ChọnTuFile.Click += new System.EventHandler(this.btn_ChonTuFile_Click);
            // 
            // txbMessage
            // 
            this.txbMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbMessage.Location = new System.Drawing.Point(114, 47);
            this.txbMessage.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.txbMessage.Multiline = true;
            this.txbMessage.Name = "txbMessage";
            this.txbMessage.Size = new System.Drawing.Size(650, 282);
            this.txbMessage.TabIndex = 41;
            // 
            // notifyIcon_SendMail
            // 
            this.notifyIcon_SendMail.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon_SendMail.Icon")));
            this.notifyIcon_SendMail.Text = "notifyIcon1";
            this.notifyIcon_SendMail.Visible = true;
            // 
            // label_CountReceiver
            // 
            this.label_CountReceiver.AutoSize = true;
            this.label_CountReceiver.Location = new System.Drawing.Point(22, 251);
            this.label_CountReceiver.Name = "label_CountReceiver";
            this.label_CountReceiver.Size = new System.Drawing.Size(69, 15);
            this.label_CountReceiver.TabIndex = 5;
            this.label_CountReceiver.Text = "Số Lượng : ";
            // 
            // Sender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 698);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Sender";
            this.tab_QuanLy.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tab_Info.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tab_SoanThu.ResumeLayout(false);
            this.tab_SoanThu.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Send;
        private System.Windows.Forms.Button btn_SeclectReceiver;
        private System.Windows.Forms.Button btn_SeclectFile;
        private System.Windows.Forms.Label label_CountEmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbSubject;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TabPage tab_QuanLy;
        private System.Windows.Forms.TabPage tab_Info;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_SoanThu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCapNhat;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Them;
        private System.Windows.Forms.TextBox tbxEmailReceiver;
        private System.Windows.Forms.ComboBox cbbGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxTimKiem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.ComboBox cbbGroup_Loc;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnCapNhatThongTin;
        private System.Windows.Forms.Button btn_DoiMatKhau;
        private System.Windows.Forms.DateTimePicker dtp_NgaySinh;
        private System.Windows.Forms.ComboBox cbbSex;
        private System.Windows.Forms.TextBox txb_SDT;
        private System.Windows.Forms.TextBox txbHoTen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txbEmail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListView lv_EmailSeclected;
        private System.Windows.Forms.Label label_So_Luong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnThemTuFile;
        private System.Windows.Forms.Button btn_ChọnTuFile;
        private System.Windows.Forms.Label lb_File;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView lv_ListReceiver;
        private System.Windows.Forms.ColumnHeader column_Email;
        private System.Windows.Forms.ColumnHeader column_Group;
        private System.Windows.Forms.TextBox txbMessage;
        private System.Windows.Forms.NotifyIcon notifyIcon_SendMail;
        private System.Windows.Forms.Label label_CountReceiver;
    }
}