﻿
namespace SendBulkEmail.GUI
{
    partial class Select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Select));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbb_Nhom = new System.Windows.Forms.ComboBox();
            this.txb_TimKiem = new System.Windows.Forms.TextBox();
            this.lv_ListEmail = new System.Windows.Forms.ListView();
            this.columnEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnGroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ckb_SelectAll = new System.Windows.Forms.CheckBox();
            this.btnXong = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbb_Nhom);
            this.panel1.Controls.Add(this.txb_TimKiem);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.ForeColor = System.Drawing.Color.Navy;
            this.panel1.Location = new System.Drawing.Point(163, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(478, 149);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(24, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tìm kiếm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(87, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lọc theo nhóm :";
            // 
            // cbb_Nhom
            // 
            this.cbb_Nhom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbb_Nhom.ForeColor = System.Drawing.Color.Navy;
            this.cbb_Nhom.FormattingEnabled = true;
            this.cbb_Nhom.Location = new System.Drawing.Point(228, 89);
            this.cbb_Nhom.Name = "cbb_Nhom";
            this.cbb_Nhom.Size = new System.Drawing.Size(121, 25);
            this.cbb_Nhom.TabIndex = 2;
            this.cbb_Nhom.SelectedIndexChanged += new System.EventHandler(this.cbb_Nhom_SelectedIndexChanged);
            // 
            // txb_TimKiem
            // 
            this.txb_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txb_TimKiem.ForeColor = System.Drawing.Color.Navy;
            this.txb_TimKiem.Location = new System.Drawing.Point(100, 41);
            this.txb_TimKiem.Name = "txb_TimKiem";
            this.txb_TimKiem.Size = new System.Drawing.Size(365, 23);
            this.txb_TimKiem.TabIndex = 0;
            this.txb_TimKiem.TextChanged += new System.EventHandler(this.txb_TimKiem_TextChanged);
            // 
            // lv_ListEmail
            // 
            this.lv_ListEmail.CheckBoxes = true;
            this.lv_ListEmail.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnEmail,
            this.columnGroup});
            this.lv_ListEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lv_ListEmail.ForeColor = System.Drawing.Color.Black;
            this.lv_ListEmail.FullRowSelect = true;
            this.lv_ListEmail.GridLines = true;
            this.lv_ListEmail.HideSelection = false;
            this.lv_ListEmail.Location = new System.Drawing.Point(163, 208);
            this.lv_ListEmail.Name = "lv_ListEmail";
            this.lv_ListEmail.Size = new System.Drawing.Size(479, 232);
            this.lv_ListEmail.TabIndex = 1;
            this.lv_ListEmail.UseCompatibleStateImageBehavior = false;
            this.lv_ListEmail.View = System.Windows.Forms.View.Details;
            // 
            // columnEmail
            // 
            this.columnEmail.Text = "Email";
            this.columnEmail.Width = 177;
            // 
            // columnGroup
            // 
            this.columnGroup.Text = "Nhóm";
            this.columnGroup.Width = 145;
            // 
            // ckb_SelectAll
            // 
            this.ckb_SelectAll.AutoSize = true;
            this.ckb_SelectAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ckb_SelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckb_SelectAll.ForeColor = System.Drawing.Color.Black;
            this.ckb_SelectAll.Location = new System.Drawing.Point(190, 182);
            this.ckb_SelectAll.Name = "ckb_SelectAll";
            this.ckb_SelectAll.Size = new System.Drawing.Size(99, 21);
            this.ckb_SelectAll.TabIndex = 2;
            this.ckb_SelectAll.Text = "Chọn tất cả";
            this.ckb_SelectAll.UseVisualStyleBackColor = false;
            this.ckb_SelectAll.CheckedChanged += new System.EventHandler(this.ckb_SelectAll_CheckedChanged);
            // 
            // btnXong
            // 
            this.btnXong.Location = new System.Drawing.Point(331, 450);
            this.btnXong.Name = "btnXong";
            this.btnXong.Size = new System.Drawing.Size(94, 39);
            this.btnXong.TabIndex = 3;
            this.btnXong.Text = "Xong";
            this.btnXong.UseVisualStyleBackColor = true;
            this.btnXong.Click += new System.EventHandler(this.btnXong_Click);
            // 
            // Select
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 506);
            this.Controls.Add(this.btnXong);
            this.Controls.Add(this.ckb_SelectAll);
            this.Controls.Add(this.lv_ListEmail);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Select";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbb_Nhom;
        private System.Windows.Forms.TextBox txb_TimKiem;
        private System.Windows.Forms.ListView lv_ListEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckb_SelectAll;
        private System.Windows.Forms.ColumnHeader columnEmail;
        private System.Windows.Forms.ColumnHeader columnGroup;
        private System.Windows.Forms.Button btnXong;
    }
}