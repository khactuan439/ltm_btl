﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Security.Cryptography;
using SendBulkEmail.DTO;

namespace SendBulkEmail.DAO
{
    class User_DAO : DataProvider_DAO
    {

        public bool Insert(User user)
        {
            if (this.search(user.Email))
                return false;
            else
            {
                string sql = string.Format("INSERT INTO user VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
                 user.Email, user.PassWord, user.PassWordEmail, user.Name, user.Sex, string.Format("{0:yyyy/MM/dd}", user.Birhday), user.Phone);
                Excute(sql);
                return true;
            }
        }
        public bool Update(User user)
        {
            if (this.search(user.Email))
                return false;
            else
            {
                string sql = string.Format("update user set password = N'{0}', password_email = '{1}', name = N'{2}', sex = {3}, birthday = '{4}', phone = '{5}' where email = '{6}'",
                user.PassWord, user.PassWordEmail, user.Name, user.Sex, string.Format("{0:yyyy/MM/dd}", user.Birhday), user.Phone, user.Email);
                Excute(sql);
                return true;
            }
        }
        public bool search(string email)
        {
            if (GetData("select* from user where email = '" + email + "'").Rows.Count > 0)
                return true;
            else return false;
        }
        // phương thức đăng nhập
        public bool login(string email, string pass)
        {
            pass = sha256.ComputeSha256Hash(pass);
            DataTable check = new DataTable();
            check = GetData(" SELECT * FROM user WHERE email = '" + email + "'");
            if (check.Rows.Count > 0)
            {
                string temp = check.Rows[0]["password"].ToString();
                if (pass == temp)
                    return true;
            }
            return false;
        }
        public bool signUp(User user)
        {
            if (this.search(user.Email))
                return false;
            else
            {
                user.PassWord = sha256.ComputeSha256Hash(user.PassWord);
                return this.Insert(user);
            }
        }
        public User GetUser(string email)
        {
            DataTable temp = GetData("select* from user where email = '" + email + "'");
            User user = new User(temp.Rows[0]);
            return user;
        }


    }
}
