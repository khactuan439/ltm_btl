﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.IO;
using System.Data;

namespace SendBulkEmail.DAO
{
    class DataProvider_DAO
    {
        private MySqlConnection conn =
            new MySqlConnection("server='localhost'; user= 'root'; database='send_bulk_email'; port=3306; password='';");
        public DataTable GetData(string mySql)
        {
            DataTable tb = new DataTable();
            this.OpenConnection();
            
                try
                {
                    MySqlCommand cmd = new MySqlCommand(mySql, this.conn);
                   
                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                MySqlCommandBuilder cm = new MySqlCommandBuilder(adapter);

                adapter.Fill(tb);
                
            }

                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                this.CloseConnection();
            return tb;
        }

        public void Excute(string mySql)
        {
            if (this.OpenConnection())
            {
                MySqlCommand command = new MySqlCommand(mySql, this.conn);
                command.ExecuteNonQuery();
            }
            this.CloseConnection();
        }

        public bool OpenConnection()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;
                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }
        public bool CloseConnection()
        {
            try
            {
                conn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}