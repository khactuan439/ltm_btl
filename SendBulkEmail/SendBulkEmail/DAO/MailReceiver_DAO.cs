﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SendBulkEmail.DTO;

namespace SendBulkEmail.DAO
{
    class MailReceiver_DAO : DataProvider_DAO
    {
        public bool Insert(MailReceiver recv)
        {
            if (GetData("select * from email_receiver where email_address = '" + recv.EmailAddress + "'").Rows.Count > 0)
                return false;
            string mySql = string.Format("Insert Into email_receiver values('{0}',N'{1}',N'{2}')", recv.EmailAddress, recv.EmailUser, recv.GroupName);
            Excute(mySql);
            return true;
        }

        public bool Update(MailReceiver recv)
        {
            if (GetData("select* from email_receiver where email_address = '" + recv.EmailAddress + "' and email_user ='" +recv.EmailUser+ "'").Rows.Count == 0)
                return false;
            string mySql = string.Format("update email_receiver set group_name = N'{0}' where email_address = '{1}' and email_user = '{2}'", recv.GroupName, recv.EmailAddress, recv.EmailUser);
            Excute(mySql);
            return true;
        }

        public bool Delete(MailReceiver emailAddress)
        {
            if (GetData("select* from email_receiver where email_address = '" + emailAddress.EmailAddress + "'").Rows.Count <= 0)
                return false;
            string mySql = string.Format(@"delete from email_receiver where email_address = '" + emailAddress.EmailAddress + "'");
            Excute(mySql);
            return true;
        }
        public DataTable getListEmail(string user)
        {
            string mySql = @"select email_address, group_name from email_receiver where email_user = '" + user + "'";
            return GetData(mySql);
        }
        public DataTable SearchEmail(string user, string email, string group)
        {
            if (string.IsNullOrEmpty(group) || group == "Tất cả")
            {
                string mySql = string.Format(@"select email_address, group_name from email_receiver where email_user = '{0}' and email_address LIKE '%{1}%'", user, email);
                return GetData(mySql);
            }
            else
            {
                string mySql = string.Format(@"select email_address, group_name from email_receiver where email_user = '{0}' and email_address LIKE '%{1}%' and group_name = '{2}'", user, email,group);
                return GetData(mySql);
            }
        }
        public DataTable getListGroup(string user)
        {
            string mySql = string.Format(@"select DISTINCT group_name from email_receiver where email_user = '{0}'", user);
            return GetData(mySql);
        }
        public List<string> ReadFile(string file )
        {
            List<string> listEmail = new List<string>();
            StreamReader sr = new StreamReader(file);
            string email;
            while ((email = sr.ReadLine()) != null)
            {
                listEmail.Add(email);
            }
            return listEmail;
        }

        internal void InsertList(List<MailReceiver> themEmail, ref int count)
        {
            foreach (var item in themEmail)
            {
                if (this.Insert(item))
                    count++;
            }
        }
        public static bool ValidateEmail(string emailaddress)
        {
            Regex email = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (email.IsMatch(emailaddress))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
