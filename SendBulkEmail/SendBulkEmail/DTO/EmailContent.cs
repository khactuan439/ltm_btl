﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using System.Net;

namespace SendBulkEmail.DTO
{
    class EmailContent
    {
        int id;
        string from;
        List<string> to;
        string subject;
        string body;
        Attachment attachment;
        DateTime dateSendEmail;

        public EmailContent()
        {
        }

        public EmailContent(int id, string from, List<string> to, string subject, string body, Attachment attachment)
        {
            this.id = id;
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
        }

        public EmailContent(int id, string from, List<string> to, string subject, string body, Attachment attachment, DateTime dateSendEmail)
        {
            this.id = id;
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
            this.dateSendEmail = dateSendEmail;
        }

        public int Id { get => id; set => id = value; }
        public string From { get => from; set => from = value; }
        public List<string> To { get => to; set => to = value; }
        public string Subject { get => subject; set => subject = value; }
        public string Body { get => body; set => body = value; }
        public Attachment Attachment { get => attachment; set => attachment = value; }
        public DateTime DateSendEmail { get => dateSendEmail; set => dateSendEmail = value; }

        // tạo nội dung mail
        public List<MailMessage> mail()
        {
            List<MailMessage> messages = new List<MailMessage>();
            foreach (var item in this.To)
            {
                MailMessage message = new MailMessage(this.From, item, this.Subject, this.Body);
                if(this.Attachment!=null)
                    message.Attachments.Add(this.Attachment);
                messages.Add(message);
            }
            
            return messages;
        }

    }
}
