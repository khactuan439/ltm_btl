﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendBulkEmail.DTO
{
    class MailReceiver
    {
        private string emailAddress;// Địa chỉ email người nhận
        private string emailUser;// Địa chỉ email nguời gửi
        private string groupName;// Nhóm được phân loại của người nhận

        public MailReceiver(string emailAddress, string emailUser, string groupName)
        {
            this.emailAddress = emailAddress;
            this.emailUser = emailUser;
            this.groupName = groupName;
        }

        public string EmailAddress { get => emailAddress; set => emailAddress = value; }
        public string EmailUser { get => emailUser; set => emailUser = value; }
        public string GroupName { get => groupName; set => groupName = value; }
    }
}
