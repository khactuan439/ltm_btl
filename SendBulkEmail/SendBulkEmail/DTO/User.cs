﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;
using System.Data;

namespace SendBulkEmail.DTO
{
    class User
    {
        private string email;
        private string passWord;//mật khẩu để đăng nhập ứng dụng Send Bulk Email
        private string passWordEmail;//mật khẩu ứng dụng để đăng nhập Gmail
        private string name;
        private string sex;
        private DateTime birhday;
        private string phone;


        public User()
        {
            this.email = "";
            this.passWord = "";
            this.passWordEmail = "";
            this.name = "";
            this.sex = "";
            this.birhday = DateTime.Now;
            this.phone = "";

        }
        public User(string email, string passWord, string passWordEmail, string name, string sex, DateTime birhday, string phone)
        {
            this.email = email;
            this.passWord = passWord;
            this.passWordEmail = passWordEmail;
            this.name = name;
            this.sex = sex;
            this.birhday = birhday;
            this.phone = phone;
        }
        public User(DataRow user)
        {
            this.email = user["email"].ToString();
            this.passWord = user["password"].ToString();
            this.passWordEmail = user["password_email"].ToString();
            this.name = user["name"].ToString();
            this.sex = user["sex"].ToString();
            this.birhday = DateTime.Parse(user["birthday"].ToString());
            this.phone = user["phone"].ToString();
        }

        public User(string name, string sex, DateTime birhday, string phone)
        {
            this.name = name;
            this.sex = sex;
            this.birhday = birhday;
            this.phone = phone;
        }

        public string Email { get => email; set => email = value; }
        public string PassWord { get => passWord; set => passWord = value; }
        public string PassWordEmail { get => passWordEmail; set => passWordEmail = value; }
        public string Name { get => name; set => name = value; }
        public string Sex { get => sex; set => sex = value; }
        public DateTime Birhday { get => birhday; set => birhday = value; }
        public string Phone { get => phone; set => phone = value; }
        public void SendMail(MailMessage message, ref int count)
        {
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(this.Email, this.PassWordEmail);
                client.Send(message);
            }
            catch (Exception)
            {
                count++;
            }
        }

    }

}
